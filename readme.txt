Self-duet is a proof-of-concept of Computer-assisted singing experience created by Julián Villegas in collaboration with Mutsuko Ishihara.

It comprises a DSP program to be run in a server, and a GUI to be run in a tablet or smartphone running TouchOSC (https://hexler.net/software/touchosc).

Recordings of the results, and more information are available at http://onkyo.u-aizu.ac.jp/software/self-duet/